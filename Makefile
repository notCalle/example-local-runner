.PHONY: clean

.DEFAULT: cache
	gitlab-runner exec docker --docker-volumes `pwd`/cache:/cache $@

cache:
	mkdir $@

clean:
	rm -rf cache
