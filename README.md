# Local debugging with gitlab-runner

This repository contains a minimal gitlab-ci project, describing how to run jobs locally using `gitlab-runner exec docker`.

## Gitlab CI configuration

The included `.gitlab-ci.yml` sets up a build pipeline with two stages, and a single job in each stage, named `build`, and `test`. The build artifact `the.thing` is cached with a common key for the `build` and `test` job.

## Result

This kind-of works, but the cache for `gitlab-runner exec` is ephemeral, and does not persist between invocations.

With a persistent volume mounted at `/cache`, it works:

```shell
$ gitlab-runner exec docker --docker-volumes `pwd`/cache:/cache ...
```

Adding a `Makefile` to wrap the details, makes everything neat and tidy:
```shell
$ make clean build test
```
